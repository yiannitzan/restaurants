export const COLOR_NAVBAR = "#3598DB";
export const COLOR_DARK_GREY = "#253640";
export const COLOR_LIGHT_GREY = "#3b5565";
export const COLOR_LIGHT = "#5f8aa3";
export const COLOR_TEXT_BLACK = "#13222C";
export const COLOR_RED = "#EE5930";
export const COLOR_BACKGROUND = "#F3F4F5";
export const COLOR_PLATINUM = "#D1E4E7";
