import firebase from "@firebase/app";
import "firebase/firestore";
import "firebase/auth";

let config = {
  apiKey: "AIzaSyB0fdd3JqH96bT34KJYVU0uNMZXsTblhhU",
  authDomain: "restaurants-app-1515723626029.firebaseapp.com",
  databaseURL: "https://restaurants-app-1515723626029.firebaseio.com",
  projectId: "restaurants-app-1515723626029",
  storageBucket: "restaurants-app-1515723626029.appspot.com",
  messagingSenderId: "886363496890"
};

firebase.initializeApp(config);

const firestore = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

export default firebase;
