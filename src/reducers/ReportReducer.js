import {
  FETCHING_REPORTS,
  FETCHING_REPORTS_SUCCESS,
  FETCHING_REPORTS_FAIL,
  SETTING_REPORT,
  SETTING_REPORT_FAIL,
  SETTING_REPORT_EXISTS,
  SETTING_REPORT_SUCCESS,
  FETCHING_REPORTS_UNSUBSCRIBE
} from "../../utils/ActionTypes";

const initialState = {
  isFetching: null,
  data: [],
  errorFetching: false,
  errorSetting: false,
  errorMessage: null,
  isSetting: null,
  settingSuccess: null
};

export default (state = initialState, action) => {
  if (action.type !== undefined) {
    switch (action.type) {
      case FETCHING_REPORTS:
        return Object.assign({}, state, {
          isFetching: true,
          data: null,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: null,
          settingSuccess: false
        });
      case FETCHING_REPORTS_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false,
          data: action.payload,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: false
        });
      case FETCHING_REPORTS_FAIL:
        return Object.assign({}, state, {
          isFetching: false,
          data: null,
          errorFetching: true,
          errorSetting: false,
          errorMessage: action.payload,
          isSetting: null
        });
      case FETCHING_REPORTS_UNSUBSCRIBE:
        return Object.assign({}, state, {
          isFetching: false,
          data: null,
          errorFetching: null,
          errorSetting: false,
          errorMessage: null,
          isSetting: null
        });

      case SETTING_REPORT:
        return Object.assign({}, state, {
          isFetching: false,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: true,
          settingSuccess: false
        });
      case SETTING_REPORT_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false,
          data: state.data,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: false,
          settingSuccess: true
        });
      case SETTING_REPORT_EXISTS:
        return Object.assign({}, state, {
          isFetching: false,
          errorFetching: false,
          errorSetting: false,
          errorMessage: action.payload.message,
          isSetting: false,
          settingSuccess: false
        });

      case SETTING_REPORT_FAIL:
        return Object.assign({}, state, {
          isFetching: false,
          errorFetching: false,
          errorSetting: true,
          errorMessage: action.payload,
          isSetting: false,
          settingSuccess: false
        });

      default:
        return state;
    }
  }
};
