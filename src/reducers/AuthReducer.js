import {
  FETCHING_USER,
  FETCHING_USER_SUCCESS,
  SETTING_USER_DNE,
  SETTING_USER_DNE_FAIL,
  FETCHING_USER_FAIL
} from "../../utils/ActionTypes";

const initialState = {
  loggedInUser: {},
  isAuthenticating: true,
  isAuthed: false,
  fetchingUser: false,
  fetchingUserFail: false
};

export default (state = initialState, action) => {
  if (action.type !== undefined) {
    switch (action.type) {
      case FETCHING_USER:
        return Object.assign({}, state, {
          loggedInUser: {},
          isAuthenticating: true,
          isAuthed: true,
          fetchingUser: true,
          fetchingUserFail: false
        });
      case FETCHING_USER_SUCCESS:
        return Object.assign({}, state, {
          loggedInUser: action.payload,
          isAuthenticating: false,
          isAuthed: true,
          fetchingUser: false,
          fetchingUserFail: false
        });
      case SETTING_USER_DNE:
        return Object.assign({}, state, {
          loggedInUser: action.payload,
          isAuthenticating: false,
          isAuthed: true,
          fetchingUser: false,
          fetchingUserFail: false
        });
      case FETCHING_USER_FAIL:
        return Object.assign({}, state, {
          loggedInUser: {},
          isAuthenticating: false,
          isAuthed: true,
          fetchingUser: false,
          fetchingUserFail: true
        });
      case SETTING_USER_DNE_FAIL:
        return Object.assign({}, state, {
          loggedInUser: {},
          isAuthenticating: false,
          isAuthed: true,
          fetchingUser: false,
          fetchingUserFail: true
        });
      default:
        return state;
    }
  }
};
