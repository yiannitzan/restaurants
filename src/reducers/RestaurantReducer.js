import {
  FETCHING_USER_RESTAURANT,
  FETCHING_USER_RESTAURANT_SUCCESS,
  FETCHING_USER_RESTAURANT_FAIL,
  FETCHING_USER_RESTAURANT_DNE,
  SETTING_USER_RESTAURANT,
  SETTING_USER_RESTAURANT_SUCCESS,
  SETTING_USER_RESTAURANT_FAIL
} from "../../utils/ActionTypes";

const initialState = {
  isFetching: null,
  data: [],
  errorFetching: false,
  errorSetting: false,
  errorMessage: null,
  isSetting: null
};

export default (state = initialState, action) => {
  if (action.type !== undefined) {
    switch (action.type) {
      case FETCHING_USER_RESTAURANT:
        return Object.assign({}, state, {
          isFetching: true,
          data: null,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: null
        });
      case FETCHING_USER_RESTAURANT_SUCCESS:
      case SETTING_USER_RESTAURANT_SUCCESS:
        return Object.assign({}, state, {
          isFetching: false,
          data: action.payload,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: false
        });
      case FETCHING_USER_RESTAURANT_DNE:
        return Object.assign({}, state, {
          isFetching: false,
          data: action.payload,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: null
        });
      case FETCHING_USER_RESTAURANT_FAIL:
        return Object.assign({}, state, {
          isFetching: false,
          data: null,
          errorFetching: true,
          errorSetting: false,
          errorMessage: action.payload,
          isSetting: null
        });

      case SETTING_USER_RESTAURANT:
        return Object.assign({}, state, {
          isFetching: false,
          data: null,
          errorFetching: false,
          errorSetting: false,
          errorMessage: null,
          isSetting: true
        });

      case SETTING_USER_RESTAURANT_FAIL:
        return Object.assign({}, state, {
          isFetching: false,
          data: null,
          errorFetching: false,
          errorSetting: true,
          errorMessage: action.payload,
          isSetting: false
        });

      default:
        return state;
    }
  }
};
