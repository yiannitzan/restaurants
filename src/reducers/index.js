import { combineReducers } from "redux";
import RestaurantReducer from "./RestaurantReducer";
import ReportReducer from "./ReportReducer";
import AuthReducer from "./AuthReducer";
import USER_LOGOUT from "../../utils/ActionTypes";

const AppReducer = combineReducers({
  restaurant: RestaurantReducer,
  reports: ReportReducer,
  auth: AuthReducer
});

const rootReducer = (state, action) => {
  if (action.type === USER_LOGOUT) {
    state = undefined;
  }

  return AppReducer(state, action);
};

export default rootReducer;
