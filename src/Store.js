import { Platform } from "react-native";
import { createStore, applyMiddleware, compose } from "redux";
// import devTools from "remote-redux-devtools";
import promise from "redux-promise";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";

//remove level false to show in logs
const logger = createLogger({
  level: {
    prevState: false,
    action: false,
    nextState: false,
    error: `error`
  },
  colors: {
    prevState: () => false,
    action: () => false,
    nextState: () => false,
    error: () => "red"
  }
});

import rootReducer from "./reducers";

const middleware = applyMiddleware(promise, thunk, logger);

const Store = createStore(rootReducer, compose(middleware));

export default Store;
