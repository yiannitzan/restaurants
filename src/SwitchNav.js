import { createStackNavigator, createSwitchNavigator } from "react-navigation";
import Home from "./components/Home/Home";
import Login from "./components/Login/Login";
import UserSettings from "./components/UserSettings/UserSettings";
import UpdateRestaurant from "./components/UserSettings/UpdateRestaurant";
import FirstLaunch from "./components/FirstLaunch/FirstLaunch";
import AuthLoadingScreen from "./components/Loading/AuthLoadingScreen";

const AppStack = createStackNavigator({
  Home: Home,
  UserSettings: UserSettings,
  UpdateRestaurant: UpdateRestaurant
});

const AuthStack = createStackNavigator({
  Login: Login,
  FirstLaunch: FirstLaunch
});

export const SwitchNav = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    AppStack: AppStack,
    AuthStack: AuthStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);

export default SwitchNav;
