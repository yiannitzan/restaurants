import {
  FETCHING_REPORTS,
  FETCHING_REPORTS_SUCCESS,
  FETCHING_REPORTS_FAIL,
  FETCHING_REPORTS_UNSUBSCRIBE
} from "../../utils/ActionTypes";
import firebase from "../../config/firebase-config";
const db = firebase.firestore();
const reportsDoc = db.collection("reports");
let unsubscribe;

export function fetchReports() {
  return dispatch => {
    let reportsData = [];
    dispatch({ type: FETCHING_REPORTS });
    unsubscribe = reportsDoc.onSnapshot(
      snapshot => {
        snapshot.forEach(doc => {
          // doc.data() is never undefined for query doc snapshots
          // console.log(doc.id, " => ", doc.data());
          reportsData.push(doc.data());
        });
        dispatch({
          type: FETCHING_REPORTS_SUCCESS,
          payload: reportsData
        });
      },
      error => {
        dispatch({ type: FETCHING_REPORTS_FAIL, payload: error });
      }
    );
  };
}

export function unsubscribeFetchingReports() {
  return dispatch => {
    dispatch({ type: FETCHING_REPORTS_UNSUBSCRIBE });
    unsubscribe();
  };
}
