import {
  FETCHING_USER_RESTAURANT,
  FETCHING_USER_RESTAURANT_SUCCESS,
  FETCHING_USER_RESTAURANT_FAIL,
  FETCHING_USER_RESTAURANT_DNE
} from "../../utils/ActionTypes";
import NavigationService from "../navigation/NavigationService";
import firebase from "../../config/firebase-config";
const db = firebase.firestore();

export default function fetchUserRestaurant(uid) {
  let userRestaurantDoc = db.collection("restaurants").doc(uid);
  return dispatch => {
    dispatch({ type: FETCHING_USER_RESTAURANT });
    userRestaurantDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          dispatch({
            type: FETCHING_USER_RESTAURANT_SUCCESS,
            payload: doc.data()
          });
          NavigationService.navigate("AppStack");
        } else {
          dispatch({ type: FETCHING_USER_RESTAURANT_DNE, payload: {} });
          NavigationService.navigate("FirstLaunch");
        }
      })
      .catch(function(error) {
        dispatch({ type: FETCHING_USER_RESTAURANT_FAIL, payload: error });
      });
  };
}
