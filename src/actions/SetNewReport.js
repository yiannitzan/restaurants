import {
  SETTING_REPORT,
  SETTING_REPORT_SUCCESS,
  SETTING_REPORT_EXISTS,
  SETTING_REPORT_FAIL
} from "../../utils/ActionTypes";
import firebase from "../../config/firebase-config";
const db = firebase.firestore();

export default function setReport(uid, report) {
  let reportsDoc = db.collection("reports").doc(uid);
  return dispatch => {
    dispatch({ type: SETTING_REPORT });
    reportsDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          error = { message: "A report has already been reported." };
          dispatch({
            type: SETTING_REPORT_EXISTS,
            payload: error
          });
        } else {
          reportsDoc
            .set(report)
            .then(success => {
              dispatch({
                type: SETTING_REPORT_SUCCESS,
                payload: report
              });
            })
            .catch(err => {
              console.log(err);
              error = { message: err };
              dispatch({
                type: SETTING_REPORT_FAIL,
                payload: error
              });
            });
        }
      })
      .catch(err => {
        error = { message: err };
        dispatch({
          type: SETTING_REPORT_FAIL,
          payload: error
        });
      });
  };
}
