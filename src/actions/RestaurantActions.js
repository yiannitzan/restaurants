import {
  UPDATING_RESTAURANT_DATES,
  UPDATING_RESTAURANT_DATES_SUCCESS,
  UPDATING_RESTAURANT_DATES_FAIL
} from "../../utils/ActionTypes";
import fetchUserRestaurant from "./FetchUserRestaurant";
import firebase from "../../config/firebase-config";
const db = firebase.firestore();

export function setInspectionDate(uid, date, type) {
  let userRestaurantDoc = db.collection("restaurants").doc(uid);
  let inspected = "";
  if (type === "last") {
    inspected = "lastInspection";
  } else {
    inspected = "nextInspection";
  }
  return dispatch => {
    dispatch({ type: UPDATING_RESTAURANT_DATES });
    userRestaurantDoc
      .update({
        inspected: date
      })
      .then(docRef => {
        console.log("restaurant inspection successfully updated!");
        dispatch({
          type: UPDATING_RESTAURANT_DATES_SUCCESS,
          payload: {}
        });
        dispatch(fetchUserRestaurant(uid));
      })
      .catch(error => {
        console.error("Error writing document: ", error);
        dispatch({
          type: UPDATING_RESTAURANT_DATES_FAIL,
          payload: error
        });
      });
  };
}
