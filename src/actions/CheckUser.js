import {
  FETCHING_USER,
  FETCHING_USER_SUCCESS,
  FETCHING_USER_FAIL,
  SETTING_USER_DNE,
  SETTING_USER_DNE_FAIL
} from "../../utils/ActionTypes";

import NavigationService from "../navigation/NavigationService";
import firebase from "../../config/firebase-config";
import fetchUserRestaurant from "./FetchUserRestaurant";
const db = firebase.firestore();

export default function checkUser(loggedInUser) {
  let userDoc = db.collection("users").doc(loggedInUser.uid);
  return dispatch => {
    dispatch({ type: FETCHING_USER });
    userDoc
      .get()
      .then(doc => {
        if (doc.exists) {
          dispatch({
            type: FETCHING_USER_SUCCESS,
            payload: loggedInUser
          });
          dispatch(fetchUserRestaurant(loggedInUser.uid));
        } else {
          userDoc
            .set(loggedInUser)
            .then(success => {
              dispatch({
                type: SETTING_USER_DNE,
                payload: loggedInUser
              });

              NavigationService.navigate("FirstLaunch");
            })
            .catch(error => {
              console.log(error);
              dispatch({
                type: SETTING_USER_DNE_FAIL,
                payload: error
              });
            });
        }
      })
      .catch(error => {
        console.log(error);
        dispatch({ type: FETCHING_USER_FAIL, payload: error });
      });
  };
}
