import {
  SETTING_USER_RESTAURANT,
  SETTING_USER_RESTAURANT_SUCCESS,
  SETTING_USER_RESTAURANT_FAIL
} from "../../utils/ActionTypes";
import fetchUserRestaurant from "./FetchUserRestaurant";
import firebase from "../../config/firebase-config";
const db = firebase.firestore();

export function setUserRestaurant(uid, restaurant) {
  let userRestaurantDoc = db.collection("restaurants").doc(uid);
  return dispatch => {
    dispatch({ type: SETTING_USER_RESTAURANT });
    userRestaurantDoc
      .set(restaurant)
      .then(docRef => {
        console.log("restaurant successfully set!");
        dispatch({
          type: SETTING_USER_RESTAURANT_SUCCESS,
          payload: {}
        });
        dispatch(fetchUserRestaurant(uid));
      })
      .catch(error => {
        console.error("Error writing document: ", error);
        dispatch({
          type: SETTING_USER_RESTAURANT_FAIL,
          payload: error
        });
      });
  };
}

export function updateUserRestaurant(uid, restaurant) {
  let userRestaurantDoc = db.collection("restaurants").doc(uid);
  return dispatch => {
    dispatch({ type: SETTING_USER_RESTAURANT });
    userRestaurantDoc
      .set(restaurant)
      .then(docRef => {
        console.log("restaurant successfully updated!");
        dispatch({
          type: SETTING_USER_RESTAURANT_SUCCESS,
          payload: restaurant
        });
      })
      .catch(error => {
        console.error("Error writing document: ", error);
        dispatch({
          type: SETTING_USER_RESTAURANT_FAIL,
          payload: error
        });
      });
  };
}
