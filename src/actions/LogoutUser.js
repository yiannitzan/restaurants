import { USER_LOGOUT } from "../../utils/ActionTypes";
import NavigationService from "../navigation/NavigationService";

export default function LogoutUser() {
  return dispatch => {
    dispatch({ type: USER_LOGOUT });
    NavigationService.navigate("AuthStack");
  };
}
