import { NavigationActions, StackActions } from "react-navigation";

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}

function getCurrentRoute() {
  let route = _navigator.state.nav;
  while (route.routes) {
    route = route.routes[route.index];
  }
  return route;
}

function reset(i, route, k) {
  _navigator.dispatch(
    StackActions.reset({
      index: i,
      actions: [NavigationActions.navigate({ routeName: route })],
      key: k
    })
  );
}

// add other navigation functions that you need and export them

export default {
  navigate,
  reset,
  setTopLevelNavigator,
  getCurrentRoute
};
