import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  ImageBackground,
  ScrollView,
  Dimensions
} from "react-native";
import { Text, Button, Icon, Card } from "react-native-elements";
import GooglePlacesInput from "../GooglePlacesInput";
import {
  COLOR_BACKGROUND,
  COLOR_TEXT_BLACK,
  COLOR_NAVBAR,
  COLOR_RED,
  COLOR_DARK_GREY,
  COLOR_PLATINUM,
  COLOR_LIGHT,
  COLOR_LIGHT_GREY
} from "../../../utils/Constants.js";

import Swiper from "react-native-swiper";
import { connect } from "react-redux";
import { setUserRestaurant } from "../../actions/SetUserRestaurant";
import GROUP from "../../../assets/images/group.png";
import SLIDE1 from "../../../assets/images/slide1.png";
import REPORT_EX from "../../../assets/images/report_overlay.png";
import MAP_EX from "../../../assets/images/map.png";
import LOCATIONS_EX from "../../../assets/images/locations.png";

const HEIGHT = Dimensions.get("window").height;
const WIDTH = Dimensions.get("window").width;

class FirstLaunch extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });
  constructor(props) {
    super(props);

    this.state = {
      continue: true,
      restaurant: null
    };

    this.handlePlacesResult = this.handlePlacesResult.bind(this);
    this.onPressContinue = this.onPressContinue.bind(this);
  }

  handlePlacesResult(data, details) {
    let userRestaurant = {
      name: details.name,
      place_id: data.place_id,
      location: details.geometry.location
    };

    // console.log(userRestaurant);
    this.setState({
      continue: false,
      restaurant: userRestaurant
    });
  }

  onPressContinue() {
    this.props.setUserRestaurant(
      this.props.auth.loggedInUser.uid,
      this.state.restaurant
    );
  }

  render() {
    let isSetting = this.props.restaurant.isSetting;

    return (
      <Swiper
        style={styles.container}
        nextButton={<Icon name="chevron-right" color={COLOR_BACKGROUND} />}
        prevButton={<Icon name="chevron-left" color={COLOR_BACKGROUND} />}
        showsButtons
        loop={false}
        activeDotColor={COLOR_BACKGROUND}
      >
        <ImageBackground source={SLIDE1} style={styles.slide1}>
          <View style={styles.card}>
            <Text style={styles.header} h2>
              Welcome
            </Text>
            <Text style={styles.cardTitle}>
              We are building a community for restaurant owners and managers
              from all over your city, to work together to solve common problems
              only they have.
            </Text>
          </View>
        </ImageBackground>

        <View style={styles.slide2}>
          <ScrollView>
            <Text style={styles.slide2Header} h4>
              Here, restaurants work together.
            </Text>

            <View>
              <Card
                title={"Map of Health Dept Inspections"}
                image={MAP_EX}
                imageStyle={styles.image3}
                imageProps={{ resizeMode: "contain" }}
                titleStyle={styles.slide2CardTitle}
                containerStyle={styles.slide2Card}
              >
                <Text style={styles.slide2Text}>
                  A common problem restaurants all have is when the Health Dept
                  will be showing up. Knowing when the Health Dept is in your
                  neighborhood around the time you expect your inspection can
                  help you better prepare for their arrival.
                </Text>
              </Card>

              <Card
                title={"Locations of Reports"}
                image={LOCATIONS_EX}
                imageStyle={styles.image1}
                imageProps={{ resizeMode: "contain" }}
                titleStyle={styles.slide2CardTitle}
                containerStyle={styles.slide2Card}
              >
                <Text style={styles.slide2Text}>
                  When the Health Dept goes out for inspections to a
                  neighborhood, they usually hit 6-8 restaurants within a 10
                  block radius all in the same day.
                </Text>
              </Card>

              <Card
                title={"Reporting"}
                image={REPORT_EX}
                imageStyle={styles.image2}
                imageProps={{ resizeMode: "contain" }}
                titleStyle={styles.slide2CardTitle}
                containerStyle={styles.slide2Card}
              >
                <Text style={styles.slide2Text}>
                  By reporting the location of the Health Dept when they are in
                  your neighborhood inspecting your restaurant, you can let
                  others know around you so they can better prepare. If everyone
                  does this, you will one day be notified they are in your
                  neighborhood when you are expecting an inspection and can
                  better prepare your own restaurant.
                </Text>
              </Card>
            </View>
          </ScrollView>
        </View>

        <View style={styles.slide3}>
          <Text
            h4
            style={{
              color: COLOR_BACKGROUND,
              marginTop: 20,
              alignItems: "center"
            }}
          >
            Please choose the restaurant you work for below to continue:
          </Text>
          <GooglePlacesInput
            handlePlacesOnPress={this.handlePlacesResult}
            styles={placesStyles}
          />

          {this.props.restaurant.errorSetting && (
            <Text style={{ color: "red" }}>
              Oops! There was an error setting your restaurant! Please try
              again!
            </Text>
          )}

          <Button
            disabled={this.state.continue}
            loading={isSetting}
            disabledStyle={styles.continueBtnDisabled}
            buttonStyle={styles.continueBtn}
            onPress={this.onPressContinue}
            title={"CONTINUE"}
          />
        </View>
      </Swiper>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  slide1: {
    flex: 1,
    justifyContent: "center",
    paddingTop: 10
  },
  card: {
    padding: 20,
    width: "100%",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0, 0.6)",
    justifyContent: "center"
  },
  cardTitle: {
    marginBottom: 10,
    fontSize: 20,
    textAlign: "center",
    color: COLOR_BACKGROUND,
    elevation: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },
  header: {
    alignSelf: "center",
    color: COLOR_BACKGROUND,
    elevation: 10,
    marginBottom: 20,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },
  image1: {
    width: "100%",
    height: 300,
    backgroundColor: "rgba(255,255,255, 0.2)"
  },
  image2: {
    width: "100%",
    height: 300,
    backgroundColor: "rgba(255,255,255, 0.2)"
  },
  image3: {
    width: "100%",
    height: 300,
    backgroundColor: "rgba(255,255,255, 0.2)"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: COLOR_DARK_GREY
  },
  slide2Header: {
    alignSelf: "center",
    color: COLOR_BACKGROUND,
    elevation: 10,
    marginTop: 30,
    marginBottom: 10,
    padding: 10,
    textAlign: "center",
    shadowColor: "#000000",
    shadowOffset: {
      width: 2,
      height: 2
    },
    shadowRadius: 10,
    shadowOpacity: 1.0
  },
  slide2Card: {
    backgroundColor: COLOR_LIGHT,
    borderRadius: 10,
    borderColor: COLOR_LIGHT_GREY,
    marginBottom: 20,
    elevation: 10,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  slide2CardTitle: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 20,
    textAlign: "center",
    color: COLOR_BACKGROUND,
    elevation: 10
  },
  slide2Text: {
    textAlign: "center",
    color: COLOR_BACKGROUND,
    elevation: 10,
    padding: 10,
    fontSize: 16
  },
  slide3: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: COLOR_DARK_GREY,
    padding: 20
  },
  slide3card: {},
  text: {
    color: COLOR_TEXT_BLACK,
    marginTop: 20
  },
  placesFormLabel: {
    marginLeft: 0,
    color: COLOR_TEXT_BLACK
  },
  placesRestaurantLabel: {
    color: COLOR_DARK_GREY,
    fontSize: 18,
    marginTop: 5,
    fontWeight: "bold"
  },
  continueBtn: {
    marginBottom: 30,
    margin: 10,
    backgroundColor: COLOR_LIGHT,
    borderRadius: 5,
    height: 48,
    width: WIDTH * 0.85,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  continueBtnDisabled: {
    opacity: 0.5,
    marginBottom: 30,
    margin: 10,
    backgroundColor: COLOR_LIGHT,
    borderRadius: 5,
    height: 48,
    width: WIDTH * 0.85,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  }
});

const placesStyles = StyleSheet.create({
  textInputContainer: {
    width: "100%",
    backgroundColor: COLOR_DARK_GREY,
    borderBottomColor: COLOR_BACKGROUND,
    borderBottomWidth: 1,
    borderTopWidth: 0,
    margin: 0,
    padding: 0,
    height: "auto"
  },
  textInput: {
    borderRadius: 0,
    backgroundColor: COLOR_DARK_GREY,
    marginLeft: 0,
    color: COLOR_BACKGROUND,
    paddingLeft: 10,
    fontSize: 18,
    height: "auto"
  },
  description: {
    fontWeight: "bold",
    color: COLOR_BACKGROUND,
    borderBottomColor: COLOR_BACKGROUND
  },
  poweredContainer: {
    backgroundColor: COLOR_DARK_GREY,
    borderBottomColor: COLOR_BACKGROUND,
    borderBottomWidth: 1
  },
  predefinedPlacesDescription: {
    color: "#1faadb"
  }
});

mapStateToProps = state => {
  return {
    restaurant: state.restaurant,
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  { setUserRestaurant }
)(FirstLaunch);
