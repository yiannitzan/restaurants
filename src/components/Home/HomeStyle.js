import { StyleSheet, Dimensions } from "react-native";
import {
  COLOR_BACKGROUND,
  COLOR_TEXT_BLACK,
  COLOR_NAVBAR,
  COLOR_RED,
  COLOR_DARK_GREY,
  COLOR_LIGHT_GREY
} from "../../../utils/Constants.js";

const HEIGHT = Dimensions.get("window").height;
const WIDTH = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    width: "100%",
    height: HEIGHT,
    zIndex: -1
  },
  slider: {
    position: "absolute",
    top: 10,
    left: 0,
    right: 0,
    marginRight: 30,
    marginLeft: 30,
    padding: 10,
    justifyContent: "flex-start",
    elevation: 0,
    zIndex: 0,
    backgroundColor: "rgba(255, 255, 255, 0.7)"
  },
  sliderTextStyle: {
    color: COLOR_TEXT_BLACK
  },
  sliderSelectedBtn: {
    backgroundColor: COLOR_RED
  },
  sliderBtnStyle: {},
  sliderContainerStyle: {
    borderRadius: 6
  },
  sliderHeaderText: {
    fontSize: 16,
    color: COLOR_DARK_GREY,
    alignSelf: "center"
  },
  reportBtn: {
    backgroundColor: COLOR_RED,
    borderRadius: 5,
    height: 48,
    width: WIDTH / 2,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  reportBtnContainer: {
    position: "absolute",
    bottom: 25,
    left: 0,
    right: 0,
    alignItems: "center",
    elevation: 0,
    zIndex: 0
  },
  avatar: {},
  avatarSelected: {
    borderColor: "red",
    borderWidth: 2
  },
  overlayHeaderText: {
    fontSize: 22,
    fontWeight: "bold",
    color: COLOR_DARK_GREY,
    alignSelf: "center"
  },
  overlayHeaderTextContainer: {
    padding: 20,
    borderBottomWidth: 2,
    borderColor: COLOR_LIGHT_GREY
  },
  overlayStyle: {
    padding: 0,
    flex: 1,
    maxHeight: 400,
    justifyContent: "space-between"
  },
  overlaySelectionContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  overlaySelection: {
    padding: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  overlayLabel: {
    color: COLOR_DARK_GREY,
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold",
    alignSelf: "center"
  },
  overlayFooter: {
    flex: 1,
    padding: 20,
    backgroundColor: COLOR_LIGHT_GREY,
    maxHeight: 100,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  overlayBtnCancel: {
    minWidth: 100,
    backgroundColor: COLOR_RED,
    alignSelf: "stretch",
    marginRight: 5,
    borderRadius: 5,
    height: 48,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  overlayBtnReport: {
    minWidth: 100,
    backgroundColor: COLOR_NAVBAR,
    alignSelf: "stretch",
    marginLeft: 5,
    borderRadius: 5,
    height: 48,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  overlayBtnReportDisabled: {
    minWidth: 100,
    opacity: 0.5,
    backgroundColor: COLOR_NAVBAR,
    alignSelf: "stretch",
    borderRadius: 5,
    marginLeft: 5,
    height: 48,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  }
});

export default styles;
