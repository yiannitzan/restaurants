import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import { Avatar, Text, Button, Overlay, Icon } from "react-native-elements";
import {
  COLOR_BACKGROUND,
  COLOR_TEXT_BLACK,
  COLOR_NAVBAR,
  COLOR_RED,
  COLOR_DARK_GREY,
  COLOR_LIGHT_GREY
} from "../../../utils/Constants.js";
import HD_ICON from "../../../assets/images/hd_icon.png";

const HEIGHT = Dimensions.get("window").height;
const WIDTH = Dimensions.get("window").width;

class ReportOverlay extends Component {
  render() {
    return (
      <Overlay
        isVisible={this.props.reportOverlayVisible}
        windowBackgroundColor="rgba(0, 0, 0, .4)"
        overlayBackgroundColor={COLOR_BACKGROUND}
        overlayStyle={this.props.styles.overlayStyle}
      >
        <View style={this.props.styles.overlayHeaderTextContainer}>
          <Text style={this.props.styles.overlayHeaderText}>
            Report location of:
          </Text>
        </View>
        <View style={this.props.styles.overlaySelectionContainer}>
          <View style={this.props.styles.overlaySelection}>
            <Avatar
              size="large"
              rounded
              overlayContainerStyle={
                !this.props.reportOptionNotSelected
                  ? this.props.styles.avatarSelected
                  : this.props.styles.avatar
              }
              source={HD_ICON}
              onPress={() => this.props.onAvatarPressed()}
              activeOpacity={0.2}
            />
            <Text style={this.props.styles.overlayLabel}>
              Health Department
            </Text>
          </View>

          <View style={this.props.styles.overlaySelection}>
            <Avatar
              size="large"
              rounded
              icon={{ name: "ellipsis-h", type: "font-awesome" }}
              onPress={() => console.log("")}
              activeOpacity={0.9}
            />
            <Text style={this.props.styles.overlayLabel}>
              More Options to come..
            </Text>
          </View>
        </View>
        {this.props.showReportError && (
          <Text style={{ color: "red" }}>
            Oops! There was an error reporting! If you have already reported
            before, you can only report once!
          </Text>
        )}
        <View style={this.props.styles.overlayFooter}>
          <Button
            onPress={() => this.props.showReport()}
            title="Cancel"
            containerStyle={{ flex: 1 }}
            buttonStyle={this.props.styles.overlayBtnCancel}
          />
          <Button
            disabled={this.props.reportOptionNotSelected}
            disabledStyle={this.props.styles.overlayBtnReportDisabled}
            onPress={() => this.props.report()}
            loading={this.props.setReportLoading}
            title="Report"
            containerStyle={{ flex: 1 }}
            buttonStyle={this.props.styles.overlayBtnReport}
          />
        </View>
      </Overlay>
    );
  }
}

export default ReportOverlay;
