import React from "react";
import { StyleSheet, Platform, View, Dimensions, Image } from "react-native";
import { MapView, Constants, Location, Permissions } from "expo";
import {
  Button,
  Text,
  Icon,
  Avatar,
  ButtonGroup,
  Slider
} from "react-native-elements";
import {
  COLOR_BACKGROUND,
  COLOR_TEXT_BLACK,
  COLOR_NAVBAR,
  COLOR_RED,
  COLOR_DARK_GREY
} from "../../../utils/Constants.js";

import styles from "./HomeStyle";
import { connect } from "react-redux";
import {
  fetchReports,
  unsubscribeFetchingReports
} from "../../actions/FetchReports";
import setReport from "../../actions/SetNewReport";

import Loading from "../Loading";
import REST_ICON2 from "../../../assets/images/restaurant_icon2.png";
import HD_ICON from "../../../assets/images/hd_icon.png";
import ReportOverlay from "./ReportOverlay";

const moment = require("moment");
const screen = Dimensions.get("window");

const ASPECT_RATIO = screen.width / screen.height;

const LATITUDE_DELTA = 0.0222;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const navStyle = StyleSheet.create({
  headerBtn: {
    backgroundColor: "transparent",
    padding: 10
  }
});

class Home extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Home",
    headerStyle: {
      backgroundColor: COLOR_NAVBAR
    },
    headerTitleStyle: {
      alignSelf: "center",
      color: "white"
    },
    headerLeft: null,
    headerRight: navigation.state.params
      ? navigation.state.params.handleHeaderLeft(navigation)
      : null
  });

  constructor(props) {
    super(props);
    this.state = {
      location: null,
      value: 1,
      currentRegion: {},
      regionChanging: false,
      reportOptionNotSelected: true,
      reportOverlayVisible: false,
      showReportError: false
    };

    options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    this.onRegionChange = this.onRegionChange.bind(this);
    this.onRegionChangeComplete = this.onRegionChangeComplete.bind(this);
    this.adjustValue = this.adjustValue.bind(this);
    this.filterReports = this.filterReports.bind(this);
  }

  componentWillMount() {
    this.props.navigation.setParams({
      handleUserSettings: () => this.props.navigation.navigate("UserSettings"),
      handleHeaderLeft: this.handleHeaderLeft,
      auth: this.props.auth
    });

    // if (Platform.OS === "android" && !Constants.isDevice) {
    //   this.setState({
    //     errorMessage: "Oops,An error has occurred, please try again!"
    //   });
    // } else {
    //   this._getLocationAsync();
    // }
  }

  componentDidMount() {
    this.getReports();
  }

  getReports() {
    this.props.fetchReports();
  }

  /**
   *
   * @param reports
   * takes all reports and filters by state.value as days ago. returns reports within that given day ago
   */
  filterReports(reports) {
    if (reports !== null) {
      let daysAgo = new Date();
      daysAgo.setDate(daysAgo.getDate() - this.state.value);
      let daysAgoOneForward = new Date();
      daysAgoOneForward.setDate(
        daysAgoOneForward.getDate() - (this.state.value - 1)
      );

      daysAgoResults = reports.filter(
        report =>
          report.timeOfReport >= daysAgo.getTime() &&
          daysAgoOneForward.getTime() >= report.timeOfReport
      );
      return daysAgoResults;
    } else {
      return [];
    }
  }

  handleHeaderLeft(navigation) {
    //If user has a picture, show avatar of user, else show blank User Icon on Btn
    if (
      navigation.state.params.auth.loggedInUser.photoURL !== null &&
      navigation.state.params.auth.loggedInUser.photoURL.length > 0
    ) {
      return (
        <View style={navStyle.headerBtn}>
          <Avatar
            size="small"
            rounded
            onPress={
              navigation.state.params
                ? navigation.state.params.handleUserSettings
                : () => null
            }
            source={{
              uri: navigation.state.params.auth.loggedInUser.photoURL
            }}
            activeOpacity={0.7}
          />
        </View>
      );
    } else {
      return (
        <Button
          onPress={
            navigation.state.params
              ? navigation.state.params.handleUserSettings
              : () => null
          }
          buttonStyle={navStyle.headerBtn}
          text={""}
          icon={
            <Icon
              name="user-circle"
              color="white"
              size={24}
              type="font-awesome"
            />
          }
        />
      );
    }
  }

  /**
   * Not in use.. perhaps needed in future
   */
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    } else {
      let location = await Location.getCurrentPositionAsync({});
      this.setState({
        location: location
      });
    }
  };

  onRegionChange(e) {
    // console.log("onRegionChange: ", e);
  }

  onRegionChangeComplete(e) {
    // console.log("onRegionChangeComplete", e);
    this.setState({
      currentRegion: e
    });
  }

  onMapReady() {
    console.log("map is ready");
  }

  adjustValue(value) {
    this.setState({
      value: value
    });
  }

  showReport() {
    this.setState({
      reportOverlayVisible: !this.state.reportOverlayVisible,
      reportOptionNotSelected: true,
      showReportError: false
    });
  }

  report() {
    console.log("Reporting at: ");
    console.log(this.props.restaurant.data);
    let report = {
      location: {
        latitude: this.props.restaurant.data.location.lat,
        longitude: this.props.restaurant.data.location.lng
      },
      placeName: this.props.restaurant.data.name,
      place_id: this.props.restaurant.data.place_id,
      type: "hd",
      timeOfReport: new Date().getTime()
    };

    this.props.setReport(this.props.auth.loggedInUser.uid, report);
  }

  componentWillReceiveProps(nextProps) {
    // console.log("home nextProps", nextProps);
    if (
      nextProps.reports.settingSuccess !== null &&
      nextProps.reports.settingSuccess
    ) {
      this.showReport();
    }

    if (nextProps.reports.errorMessage !== null) {
      this.setState({
        showReportError: true
      });
    }
  }

  componentWillUnmount() {
    /** Not unsubscribing from here because react navigation doesnt call this on screen change.
     * Instead calling unsubscribe when user logs out.
     * Flow of events from logout: logout btn press -> unsubscribe called -> firebase auth logged out
     *  -> navigate to login -> (Home gets unmounted now, throws firebase permission errors if unsub called here)
     * May have better solution in future..
     */
    // this.props.unsubscribeFetchingReports();
  }

  onAvatarPressed() {
    this.setState({
      reportOptionNotSelected: !this.state.reportOptionNotSelected
    });
  }

  render() {
    let user_restaurant = this.props.restaurant.data;
    let user_restaurant_region = {};

    if (user_restaurant != null) {
      user_restaurant_region = {
        latitude: user_restaurant.location.lat,
        longitude: user_restaurant.location.lng,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      };
    }

    let reports = this.filterReports(this.props.reports.data);

    if (user_restaurant === null || reports === null) {
      return <Loading />;
    } else {
      return (
        <View style={styles.container}>
          <MapView
            style={styles.map}
            provider={"google"}
            loadingEnabled={true}
            initialRegion={user_restaurant_region}
            onMapReady={this.onMapReady}
            onRegionChange={e => this.onRegionChange(e)}
            onRegionChangeComplete={e => this.onRegionChangeComplete(e)}
            // showsUserLocation={this.state.location !== null}
            // showsMyLocationButton={this.state.location !== null}
          >
            <MapView.Marker
              coordinate={{
                latitude: user_restaurant.location.lat + 0.00001,
                longitude: user_restaurant.location.lng + 0.00001
              }}
              title={user_restaurant.name}
              description={"Your Restaurant"}
              image={Platform.OS === "android" ? REST_ICON2 : undefined}
            >
              {Platform.OS === "ios" && (
                <Image source={REST_ICON2} style={{ width: 48, height: 48 }} />
              )}
            </MapView.Marker>
            {reports !== undefined &&
              reports.length > 0 &&
              reports.map((marker, index) => {
                let reportTitle = "";
                if (marker.placeName !== undefined) {
                  reportTitle =
                    "Health Department Reported at " + marker.placeName;
                } else {
                  reportTitle = "Health Department Reported";
                }
                return (
                  <MapView.Marker
                    key={index}
                    coordinate={marker.location}
                    title={reportTitle}
                    description={moment(new Date(marker.timeOfReport)).format(
                      "MM-DD-YYYY"
                    )}
                    image={Platform.OS === "android" ? HD_ICON : undefined}
                  >
                    {Platform.OS === "ios" && (
                      <Image
                        source={HD_ICON}
                        style={{ width: 48, height: 48 }}
                      />
                    )}
                  </MapView.Marker>
                );
              })}
          </MapView>

          <View style={styles.slider}>
            <Text style={styles.sliderHeaderText}>Showing reports from: </Text>
            <Slider
              value={this.state.value}
              maximumValue={30}
              minimumValue={1}
              step={1}
              onValueChange={value => this.adjustValue(value)}
            />
            {this.state.value > 1 ? (
              <Text style={styles.sliderHeaderText}>
                {this.state.value} days ago
              </Text>
            ) : (
              <Text style={styles.sliderHeaderText}>
                {this.state.value} day ago - Now
              </Text>
            )}
          </View>

          <Button
            onPress={() => this.showReport()}
            title="Report"
            buttonStyle={styles.reportBtn}
            containerStyle={styles.reportBtnContainer}
          />

          <ReportOverlay
            reportOverlayVisible={this.state.reportOverlayVisible}
            reportOptionNotSelected={this.state.reportOptionNotSelected}
            styles={styles}
            setReportLoading={this.props.reports.isSetting}
            showReport={() => this.showReport()}
            report={() => this.report()}
            showReportError={this.state.showReportError}
            onAvatarPressed={() => this.onAvatarPressed()}
          />
        </View>
      );
    }
  }
}

mapStateToProps = state => {
  return {
    restaurant: state.restaurant,
    reports: state.reports,
    auth: state.auth
  };
};

//Another way to use actions , instead of just declaring directly in connect() below
const mapDispatchToProps = dispatch => {
  return {
    setReport: (uid, report) => dispatch(setReport(uid, report)),
    fetchReports: () => dispatch(fetchReports()),
    unsubscribeFetchingReports: () => dispatch(unsubscribeFetchingReports())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
