import React, { Component } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import {
  Text,
  Button,
  Avatar,
  FormLabel,
  Icon,
  Overlay,
  Card
} from "react-native-elements";
import * as Color from "../../../utils/Constants.js";

import GooglePlacesInput from "../GooglePlacesInput";

import { connect } from "react-redux";
import { updateUserRestaurant } from "../../actions/SetUserRestaurant";
const HEIGHT = Dimensions.get("window").height;
const WIDTH = Dimensions.get("window").width;

class UpdateRestaurant extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "",
    headerStyle: {
      backgroundColor: Color.COLOR_NAVBAR
    },
    headerTintColor: Color.COLOR_BACKGROUND,
    headerTitleStyle: {
      color: Color.COLOR_BACKGROUND
    }
  });

  constructor(props) {
    super(props);

    this.state = {
      updatedRestaurant: null,
      sameRestaurantError: false
    };

    this.handlePlacesResult = this.handlePlacesResult.bind(this);
    this.setRestaurant = this.setRestaurant.bind(this);
  }

  setRestaurant() {
    let updatedRestaurant = this.state.updatedRestaurant;
    if (
      updatedRestaurant !== null &&
      this.props.restaurant.data.place_id !== updatedRestaurant.place_id
    ) {
      console.log("updating restaurant with: ", updatedRestaurant);
      this.props.updateUserRestaurant(
        this.props.auth.loggedInUser.uid,
        this.state.updatedRestaurant
      );
    } else {
      this.setState({
        sameRestaurantError: true,
        updatedRestaurant: null
      });
    }
  }

  handlePlacesResult(data, details) {
    let userRestaurant = {
      name: details.name,
      place_id: data.place_id,
      location: details.geometry.location
    };

    console.log(userRestaurant);
    this.setState({
      updatedRestaurant: userRestaurant,
      sameRestaurantError: false
    });
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.restaurant.data !== null &&
      this.state.updatedRestaurant.place_id ==
        nextProps.restaurant.data.place_id
    ) {
      this.props.navigation.goBack();
    }
  }

  render() {
    const restaurant = this.props.restaurant.data;
    let isSetting = this.props.restaurant.isSetting;

    let updatedRestaurantNull = true;
    if (this.state.updatedRestaurant !== null) {
      updatedRestaurantNull = false;
    }

    return (
      <View style={styles.container}>
        <Text
          h4
          style={{
            color: Color.COLOR_BACKGROUND,
            marginTop: 10,
            alignItems: "center"
          }}
        >
          Update Restaurant:
        </Text>
        <GooglePlacesInput
          handlePlacesOnPress={this.handlePlacesResult}
          styles={placesStyles}
        />

        {this.state.sameRestaurantError && (
          <Text style={{ color: "red", alignItems: "center" }}>
            Oops! Looks like you selected the same restaurant to update to!
          </Text>
        )}

        {this.props.restaurant.errorSetting && (
          <Text style={{ color: "red", alignItems: "center" }}>
            Oops! There was an error setting your restaurant! Please try again!
          </Text>
        )}

        <Button
          disabled={this.updatedRestaurantNull}
          onPress={this.setRestaurant}
          loading={isSetting}
          title="Update"
          buttonStyle={
            updatedRestaurantNull ? styles.updateBtnDisabled : styles.updateBtn
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: Color.COLOR_DARK_GREY,
    padding: 20
  },
  text: {
    color: Color.COLOR_TEXT_BLACK,
    marginTop: 20
  },
  updateBtn: {
    marginBottom: 30,
    margin: 10,
    backgroundColor: Color.COLOR_LIGHT,
    borderRadius: 5,
    height: 48,
    width: WIDTH * 0.85,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  updateBtnDisabled: {
    opacity: 0.5,
    marginBottom: 30,
    margin: 10,
    backgroundColor: Color.COLOR_LIGHT,
    borderRadius: 5,
    height: 48,
    width: WIDTH * 0.85,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  }
});

const placesStyles = StyleSheet.create({
  textInputContainer: {
    width: "100%",
    backgroundColor: Color.COLOR_DARK_GREY,
    borderBottomColor: Color.COLOR_BACKGROUND,
    borderBottomWidth: 1,
    borderTopWidth: 0,
    margin: 0,
    padding: 0,
    height: "auto"
  },
  textInput: {
    borderRadius: 0,
    backgroundColor: Color.COLOR_DARK_GREY,
    marginLeft: 0,
    color: Color.COLOR_BACKGROUND,
    paddingLeft: 10,
    fontSize: 18,
    height: "auto"
  },
  description: {
    fontWeight: "bold",
    color: Color.COLOR_BACKGROUND,
    borderBottomColor: Color.COLOR_BACKGROUND
  },
  poweredContainer: {
    backgroundColor: Color.COLOR_DARK_GREY,
    borderBottomColor: Color.COLOR_BACKGROUND,
    borderBottomWidth: 1
  },
  predefinedPlacesDescription: {
    color: "#1faadb"
  }
});

mapStateToProps = state => {
  return {
    restaurant: state.restaurant,
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  { updateUserRestaurant }
)(UpdateRestaurant);
