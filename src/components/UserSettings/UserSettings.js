import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  ScrollView,
  Share,
  Linking
} from "react-native";
import { Text, Button, Avatar, Icon, Card } from "react-native-elements";
import { FontAwesome, MaterialIcons } from "@expo/vector-icons";
import Loading from "../Loading";
import { COLOR_BACKGROUND, COLOR_NAVBAR } from "../../../utils/Constants.js";

import styles from "./UserSettingsStyle";
import { connect } from "react-redux";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import LogoutUser from "../../actions/LogoutUser";
import { unsubscribeFetchingReports } from "../../actions/FetchReports";
import { updateUserRestaurant } from "../../actions/SetUserRestaurant";

import firebase from "../../../config/firebase-config";
import DatePicker from "react-native-datepicker";

const moment = require("moment");

class UserSettings extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "User Settings",
    headerStyle: {
      backgroundColor: COLOR_NAVBAR
    },
    headerTintColor: COLOR_BACKGROUND,
    headerTitleStyle: {
      color: COLOR_BACKGROUND
    }
  });

  constructor(props) {
    super(props);

    let today = moment(new Date()).format("MM-DD-YYYY");
    let lastInspected = props.restaurant.data.lastInspection
      ? moment(props.restaurant.data.lastInspection).format("MM-DD-YYYY")
      : today;
    let nextInspected = props.restaurant.data.nextInspection
      ? moment(props.restaurant.data.nextInspection).format("MM-DD-YYYY")
      : today;
    this.state = {
      date: today,
      lastInspected: lastInspected,
      nextInspected: nextInspected
    };

    this.onDateChange = this.onDateChange.bind(this);
  }

  shareApp() {
    if (Platform.OS === "ios") {
      Share.share(
        {
          message:
            "A “Waze” for restaurant owners and managers to report the location of the health department.",
          url: "https://therestaurantcommunity.herokuapp.com/",
          title: "The Restaurant Community App"
        },
        {}
      );
    } else {
      //android
      Share.share(
        {
          message:
            "A “Waze” for restaurant owners and managers to report the location of the health department.",
          url: "https://therestaurantcommunity.herokuapp.com/", //url doesnt work on android
          title: "The Restaurant Community App"
        },
        {
          // Android only:
          dialogTitle: "Share The Restaurant Community with Friends"
        }
      );
    }
  }

  logout() {
    this.props.unsubscribeFetchingReports();
    const thisComp = this; //either declare this outside funciton, or use arrow function below. or else this refers to functions this below.
    firebase
      .auth()
      .signOut()
      .then(function() {
        // Sign-out successful.\
        thisComp.props.LogoutUser();
      })
      .catch(function(error) {
        // An error happened.
        console.log(error);
      });
  }

  onDateChange(date, type) {
    let restaurant = this.props.restaurant.data;
    let dateInMillis = moment(date, "MM-DD-YYYY").valueOf();
    console.log("onDateChanged: ", dateInMillis, type);
    if (type === "last") {
      restaurant["lastInspection"] = dateInMillis;
      this.setState({
        lastInspected: date
      });
    } else if (type === "next") {
      restaurant["nextInspection"] = dateInMillis;
      this.setState({
        nextInspected: date
      });
    }

    this.props.updateUserRestaurant(
      this.props.auth.loggedInUser.uid,
      restaurant
    );
  }

  goToUpdateRestaurant() {
    this.props.navigation.navigate("UpdateRestaurant");
  }

  render() {
    const user = this.props.auth.loggedInUser;
    const restaurant = this.props.restaurant.data;
    console.log(restaurant);
    let isSetting = this.props.restaurant.isSetting;

    if (user && restaurant) {
      return (
        <View style={styles.container}>
          <KeyboardAwareScrollView
            ref={ref => (this.scrollView = ref)}
            style={{ flex: 1 }}
            enableOnAndroid={true}
            extraHeight={250}
          >
            <Card containerStyle={styles.userCard}>
              <View style={styles.avatarContainer}>
                <Avatar
                  size="xlarge"
                  rounded
                  source={{ uri: user.photoURL }}
                  onPress={() => console.log("Works!")}
                  activeOpacity={0.7}
                  avatarStyle={styles.avatar}
                />
              </View>
              <View style={styles.contentContainer}>
                <Text style={styles.labelStyle}>Full Name</Text>
                <Text style={styles.textStyle}>{user.fullName}</Text>

                <Text style={styles.labelStyle}>Email</Text>
                <Text style={styles.textStyle}>{user.email}</Text>
              </View>
            </Card>

            <Card containerStyle={styles.restaurantCard}>
              <Text style={styles.labelStyle}>Restaurant</Text>
              <View>
                <Text style={styles.restaurantTextStyle}>
                  {restaurant.name}
                </Text>
                <Button
                  onPress={() => this.goToUpdateRestaurant()}
                  buttonStyle={styles.restaurantEditBtn}
                  containerStyle={{
                    alignSelf: "flex-start"
                  }}
                  titleStyle={styles.restaurantEditBtnText}
                  title="EDIT"
                />
              </View>
              <InspectionDatePicker
                type="last"
                date={this.state.lastInspected}
                title="Last Inspection:"
                onDateChange={this.onDateChange}
              />
              <InspectionDatePicker
                type="next"
                date={this.state.nextInspected}
                title="Next Expected Inspection:"
                onDateChange={this.onDateChange}
              />
            </Card>

            <View style={styles.footer}>
              <Button
                onPress={() => this.shareApp()}
                icon={<Icon name="share" color="white" type="entypo" />}
                title="Share App with Friends!"
                buttonStyle={styles.shareBtn}
              />

              <Button
                onPress={() => this.logout()}
                icon={<Icon name="log-out" color="white" type="entypo" />}
                title="Log Out"
                buttonStyle={styles.logoutBtn}
              />

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                  paddingTop: 25
                }}
              >
                <Text
                  style={styles.footerTxt}
                  onPress={() => {
                    Linking.openURL(
                      "https://therestaurantcommunity.herokuapp.com/app/privacy-policy"
                    );
                  }}
                >
                  Privacy Policy
                </Text>

                <Text
                  style={styles.footerTxt}
                  onPress={() => {
                    Linking.openURL(
                      "https://therestaurantcommunity.herokuapp.com/app/terms"
                    );
                  }}
                >
                  Terms & Conditions
                </Text>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      );
    } else {
      return <Loading />;
    }
  }
}

const InspectionDatePicker = props => {
  return (
    <View style={{ marginVertical: 10 }}>
      <Text style={styles.labelStyle}>{props.title}</Text>
      <DatePicker
        style={{ width: "auto", padding: 5 }}
        date={props.date}
        mode="date"
        placeholder="Select Date"
        format="MM-DD-YYYY"
        minDate="01-01-2017"
        maxDate="12-31-2019"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: "absolute",
            left: 0,
            marginLeft: 0
          },
          dateInput: {}
          // ... You can check the source to find the other keys.
        }}
        onDateChange={date => props.onDateChange(date, props.type)}
      />
    </View>
  );
};

mapStateToProps = state => {
  return {
    restaurant: state.restaurant,
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  {
    LogoutUser,
    unsubscribeFetchingReports,
    updateUserRestaurant
  }
)(UserSettings);
