import { StyleSheet, Dimensions } from "react-native";
import {
  COLOR_BACKGROUND,
  COLOR_TEXT_BLACK,
  COLOR_NAVBAR,
  COLOR_RED,
  COLOR_DARK_GREY,
  COLOR_LIGHT,
  COLOR_LIGHT_GREY
} from "../../../utils/Constants.js";

const HEIGHT = Dimensions.get("window").height;
const WIDTH = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR_BACKGROUND
  },
  contentContainer: {
    flex: 1,
    padding: 10,
    paddingBottom: 16
  },
  userCard: {
    padding: 0,
    margin: 20,
    elevation: 8,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  avatarContainer: {
    flex: 1,
    minHeight: 200,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    overflow: "hidden",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: COLOR_DARK_GREY
  },
  avatar: {
    alignSelf: "center",
    justifyContent: "center"
  },
  labelStyle: {
    color: COLOR_LIGHT,
    fontSize: 18,
    paddingLeft: 8,
    paddingRight: 8,
    marginBottom: 4,
    fontWeight: "bold"
  },
  textStyle: {
    marginBottom: 8,
    paddingLeft: 8,
    paddingRight: 8,
    fontSize: 18,
    color: COLOR_DARK_GREY
  },
  restaurantCard: {
    padding: 10,
    margin: 20,
    elevation: 8,
    borderRadius: 5,
    borderWidth: 0,
    borderColor: "white",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  restaurantTextStyle: {
    paddingLeft: 8,
    fontSize: 18,
    color: COLOR_DARK_GREY
  },
  restaurantEditBtn: {
    borderRadius: 1,
    margin: 8,
    marginTop: 16,
    padding: 0,
    minWidth: 88,
    elevation: 2,
    borderWidth: 0.5,
    borderColor: "black",
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5,
    backgroundColor: "white"
  },
  restaurantEditBtnText: {
    color: COLOR_DARK_GREY,
    fontWeight: "normal",
    fontSize: 18
  },
  footer: {
    flex: 1,
    alignItems: "stretch",
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 40,
    paddingBottom: 40,
    marginTop: 50,
    backgroundColor: COLOR_DARK_GREY
  },
  footerTxt: {
    color: "white"
  },
  logoutBtn: {
    margin: 10,
    height: 48,
    borderRadius: 5,
    backgroundColor: COLOR_RED,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  shareBtn: {
    margin: 10,
    height: 48,
    borderRadius: 5,
    backgroundColor: COLOR_NAVBAR,
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  }
});

export default styles;
