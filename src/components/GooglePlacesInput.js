import React, { Component } from "react";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";

const GooglePlacesInput = props => {
  return (
    <GooglePlacesAutocomplete
      placeholder="Search For Restaurant Here"
      minLength={2} // minimum length of text to search
      autoFocus={false}
      onFocus={() => console.log("on focus")}
      returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed="true" // true/false/undefined
      fetchDetails={true}
      renderDescription={row => row.description} // custom description render
      onPress={(data, details) => {
        // 'details' is provided when fetchDetails = true
        props.handlePlacesOnPress(data, details);
      }}
      getDefaultValue={() => {
        return "";
      }}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: "AIzaSyAcNvIJHslFm11fHuo9rJIyltrwZ33a9GU",
        language: "en", // language of the results
        types: "establishment" // default: 'geocode'
      }}
      styles={props.styles}
      currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
      currentLocationLabel="Current location"
      nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
      GoogleReverseGeocodingQuery={
        {
          // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
        }
      }
      GooglePlacesSearchQuery={{
        // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
        rankby: "distance",
        types: "food"
      }}
      filterReverseGeocodingByTypes={[
        "locality",
        "administrative_area_level_3"
      ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
      debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
    />
  );
};

export default GooglePlacesInput;
