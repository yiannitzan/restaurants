import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { Text } from "react-native-elements";
import { DangerZone } from "expo";
const ANIMATION = require("../../utils/3d_circle_loader.json");
const { Lottie } = DangerZone;

class Loading extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.animation.play();
  }

  render() {
    return (
      <Lottie
        loop={true}
        ref={animation => {
          this.animation = animation;
        }}
        style={styles.lottie}
        source={ANIMATION}
      />
    );
  }
}

const styles = StyleSheet.create({
  lottie: {
    flex: 1,
    backgroundColor: "#333"
  }
});

export default Loading;
