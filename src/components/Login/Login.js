import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  ImageBackground
} from "react-native";
import { Text, Button, SocialIcon } from "react-native-elements";
import firebase from "../../../config/firebase-config";
import Loading from "../Loading";
import {
  COLOR_NAVBAR,
  COLOR_DARK_GREY,
  COLOR_RED,
  COLOR_BACKGROUND
} from "../../../utils/Constants";
import LOGO from "../../../assets/images/logo_trans.png";
import BG from "../../../assets/images/bg.png";
import { connect } from "react-redux";
import checkUser from "../../actions/CheckUser";

const HEIGHT = Dimensions.get("window").height;
const WIDTH = Dimensions.get("window").width;

class Login extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  stopLoading() {
    this.setState({
      isLoading: false
    });
  }

  startLoading() {
    this.setState({
      isLoading: true
    });
  }

  googleLogin = async () => {
    this.startLoading();
    try {
      const result = await Expo.Google.logInAsync({
        webClientId:
          "886363496890-nnfec523hcktdnfn76bud9uqg6tmi4hj.apps.googleusercontent.com",
        androidClientId:
          "886363496890-s6tjrtdqb9cdfna9512fn71bh1in8ego.apps.googleusercontent.com",
        androidStandaloneAppClientId:
          "886363496890-gve8ktjfklkeouf2ds949rdh8brsg2k7.apps.googleusercontent.com",
        iosStandaloneAppClientId:
          "886363496890-i9smt2d0lead05q48khf5h3ei3gvrrkl.apps.googleusercontent.com",
        iosClientId:
          "886363496890-53o7anigv7hpam3solg7ann5o8p3narn.apps.googleusercontent.com",
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        console.log("google sign in success");
        console.log(result);
        const credential = firebase.auth.GoogleAuthProvider.credential(
          result.idToken,
          result.accessToken
        );

        // Sign in with credential from the Google user.
        firebase
          .auth()
          .signInAndRetrieveDataWithCredential(credential)
          .then(result => {
            console.log("successful login with google!");
            console.log(result);
          })
          .catch(error => {
            // Handle Errors here.
            console.log("Error occurred Logging in!");
            console.log(error);
            this.stopLoading();
          });
      } else {
        this.stopLoading();
        return { cancelled: true };
      }
    } catch (e) {
      this.stopLoading();
      return { error: true };
    }
  };

  fbLogin = async () => {
    this.startLoading();
    const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync(
      "926114817536961",
      {
        permissions: ["public_profile", "email"]
      }
    );
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      const credential = firebase.auth.FacebookAuthProvider.credential(token);

      // Sign in with credential from the Facebook user.
      firebase
        .auth()
        .signInAndRetrieveDataWithCredential(credential)
        .then(result => {
          console.log("successful login with FB!");
          console.log(result);
        })
        .catch(error => {
          // Handle Errors here.
          console.log("Error occurred logging in: ", error);
          this.stopLoading();
        });
    } else {
      this.stopLoading();
    }
  };

  render() {
    if (this.state.isLoading) {
      return <Loading />;
    } else {
    }
    return (
      <ImageBackground source={BG} style={styles.container}>
        <Image
          style={styles.logoContainer}
          source={LOGO}
          resizeMode={"cover"}
        />

        <View style={styles.loginContainer}>
          <SocialIcon
            title="Sign In With Google"
            button
            type="google"
            style={styles.googleBtn}
            onPress={() => this.googleLogin()}
          />

          <SocialIcon
            title="Sign In With Facebook"
            button
            type="facebook"
            style={styles.fbBtn}
            onPress={() => this.fbLogin()}
          />

          {/* <SocialIcon
            title="Sign In With Email"
            button
            type="envelope"
            iconColor={"black"}
            style={styles.emailBtn}
            fontStyle={{ color: "black" }}
            onPress={() => this.logIn()}
          /> */}
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between"
  },
  logoContainer: {
    height: HEIGHT / 3,
    width: WIDTH
  },
  welcomeTxt: {
    color: COLOR_BACKGROUND,
    paddingLeft: 20
  },
  loginContainer: {
    maxHeight: HEIGHT / 3,
    backgroundColor: "rgba(0,0,0, 0.6)",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20
  },
  googleBtn: {
    backgroundColor: COLOR_RED,
    alignSelf: "stretch",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  fbBtn: {
    alignSelf: "stretch",
    elevation: 2,
    shadowColor: "#000000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5
  },
  emailBtn: {
    backgroundColor: COLOR_BACKGROUND
  }
});

mapStateToProps = state => {
  return {
    restaurant: state.restaurant,
    auth: state.auth
  };
};

export default connect(mapStateToProps, { checkUser })(Login);
