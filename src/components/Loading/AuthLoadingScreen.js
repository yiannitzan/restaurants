import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import { Text } from "react-native-elements";
import { DangerZone, Font, Asset } from "expo";
const ANIMATION = require("../../../utils/3d_circle_loader.json");
const { Lottie } = DangerZone;
import firebase from "../../../config/firebase-config";
import checkUser from "../../actions/CheckUser";
import { connect } from "react-redux";

function cacheImages(images) {
  return images.map(image => {
    return Asset.fromModule(image).downloadAsync();
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => {
    Font.loadAsync(font);
  });
}

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
  }

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require("../../../assets/images/bg.png"),
      require("../../../assets/images/slide1.png"),
      require("../../../assets/images/logo_trans.png"),
      require("../../../assets/images/locations.png"),
      require("../../../assets/images/map.png"),
      require("../../../assets/images/report_overlay.png")
    ]);

    const fontAssets = cacheFonts([
      {
        roboto: require("../../../assets/fonts/Roboto-Regular.ttf")
      }
    ]);

    await Promise.all([...imageAssets, ...fontAssets]);
  }

  async componentDidMount() {
    this.animation.play();
    await this._loadAssetsAsync();

    let thisComp = this;
    firebase.auth().onAuthStateChanged(
      user => {
        if (user) {
          console.log("we are authenticated now!");
          let loggedInUser = {
            fullName: user.displayName,
            email: user.email,
            photoURL: user.providerData[0].photoURL,
            uid: user.uid
          };
          thisComp.props.checkUser(loggedInUser);
        } else {
          console.log("not authenticated!");
          thisComp.props.navigation.navigate("AuthStack");
        }
      },
      error => {
        console.log("Error authenticating: ", error);
        thisComp.props.navigation.navigate("AuthStack");
      }
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.fetchingUserFail) {
      this.props.navigation.navigate("AuthStack");
    }
  }

  render() {
    return (
      <Lottie
        loop={true}
        ref={animation => {
          this.animation = animation;
        }}
        style={styles.lottie}
        source={ANIMATION}
      />
    );
  }
}

const styles = StyleSheet.create({
  lottie: {
    flex: 1,
    backgroundColor: "#333"
  }
});

mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  { checkUser }
)(AuthLoadingScreen);
