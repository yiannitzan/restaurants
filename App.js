import React from "react";
import { BackHandler } from "react-native";
import Store from "./src/Store";
import { Provider } from "react-redux";
import NavigationService from "./src/navigation/NavigationService";
import SwitchNav from "./src/SwitchNav";
import Sentry from "sentry-expo";

Sentry.enableInExpoDevelopment = false;

Sentry.config(
  "https://0ba4feae65ff48be8bad1824667e49aa@sentry.io/1288522"
).install();

export default class App extends React.Component {
  constructor(props) {
    super(props);
    console.ignoredYellowBox = ["Setting a timer"];
  }

  componentDidMount() {
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.onBackPress.bind(this)
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.onBackPress.bind(this)
    );
  }

  onBackPress() {
    console.log("onBackPress");
    let activeRoute = NavigationService.getCurrentRoute();

    if (activeRoute.routeName === "Home" || activeRoute.routeName === "Login") {
      //let apploading checks handle where to go next.
      BackHandler.exitApp();
      return true;
    } else {
      return false;
    }
  }

  render() {
    return (
      <Provider store={Store}>
        <SwitchNav
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}
