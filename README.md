A “Waze” for restaurant owners and managers to report the location of the health department.

Funcionality includes:

* Logging in with Google/Facebook
* Authentication with Firebase
* Redux for state management
* Google places Search with google places api
* Google Maps view with Mapview
* Reporting Location of Health Department, stored in Firebase to User
* User Settings page to edit user restaurant, logout and share app with friends.

This project runs with Expo, to run on your device or simulator please follow the instructions provided by Expo @ https://docs.expo.io/versions/latest/introduction/installation

Project is live for testing both app stores: 

* Google Play Store - https://play.google.com/store/apps/details?id=com.therestaurantcommunity

* iOS App Store - https://itunes.apple.com/us/app/the-restaurant-community/id1372419138?ls=1&mt=8

Project Structure: 

* Redux Actions - `src/actions`

* Redux Reducers - `src/reducers`

* React Navigation - `src/SwitchNav.js`

* Screens - `src/components` - 4 main screens - Login, FirstLaunch, Home, UserSettings & Loading/Splash

Any questions feel free to reach out to my email @ johntzanida@gmail.com